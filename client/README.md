# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh


Note:- While setting-up this project if you cloned open 2 terminalsin vs code one with root directory and other the client directory with the commande cd client then just run the command npm run dev in both the terminals. the app will started on the localhost 5173

Note:- I have built proxy server too for better developer experience. Also i have added the site on render so it visible directly with the following link https://review-app-yd87.onrender.com/ 