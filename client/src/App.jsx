import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ReviewList from "./components/ReviewList";
export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<ReviewList />} />
      </Routes>
    </BrowserRouter>
  );
}
