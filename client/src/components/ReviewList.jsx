import React, { useState, useEffect } from "react";
import "../App.css";
import { IoPersonAddOutline } from "react-icons/io5";
import { FaRegBookmark } from "react-icons/fa";
import { BsThreeDots } from "react-icons/bs";
import ReviewHighlighter from "./ReviewHighlighter";
import StarRating from "./StarRating";

export default function ReviewList() {
  const [reviews, setReviews] = useState([]);

  const fetchReviews = async () => {
    try {
      const response = await fetch("/api/reviewlist/getreview", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (!response.ok) {
        console.error("Network response was not ok:", response.status);
        return;
      }

      const result = await response.json();
      setReviews(result);
    } catch (error) {
      console.error(
        "There has been a problem with your fetch operation:",
        error
      );
    }
  };
  useEffect(() => {
    fetchReviews();
  }, []);

  return (
    <section>
      {reviews.map((review, index) => (
        <div key={index} className="review-list">
          <div id="icon">
            <img src={review.source.icon} alt="thumbnail" />
          </div>
          <div className="review-col">
            <div className="review-head">
              <p>
                <strong>{review.reviewer_name}</strong> wrote a review at{" "}
                <strong>{review.source.name}</strong>
              </p>{" "}
              <strong className="gap-p">
                <IoPersonAddOutline /> <FaRegBookmark /> <BsThreeDots />
              </strong>
            </div>
            <div className="review-rating">
              <StarRating rating={review.rating_review_score / 2} />{" "}
              {review.date}
            </div>
            <div className="review-desc">
              <ReviewHighlighter review={review} />
            </div>
          </div>
        </div>
      ))}
    </section>
  );
}
