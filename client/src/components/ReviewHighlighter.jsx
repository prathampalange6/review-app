import React from "react";

const sentimentColors = {
  Positive: "#C8E6C9",
  Negative: "#FFCDD2",
  Neutral: "#FFE0B2",
};

const ReviewHighlighter = ({ review }) => {
  const { analytics, content } = review;

  let highlightedContent = content;
  analytics.forEach(({ highlight_indices, sentiment, topic }) => {
    highlight_indices.forEach(([start, end]) => {
      const sentence = highlightedContent.slice(start, end);
      const highlightedSentence = `<span style="background-color: ${sentimentColors[sentiment]};" title="${topic}">${sentence}</span>`;
      highlightedContent = highlightedContent.replace(
        sentence,
        highlightedSentence
      );
    });
  });

  return (
    <div
      className="review-highlighter"
      dangerouslySetInnerHTML={{ __html: highlightedContent }}
    />
  );
};

export default ReviewHighlighter;