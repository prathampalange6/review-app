import React from 'react';
import { IoStarOutline, IoStarSharp } from 'react-icons/io5';

function StarRating({ rating }) {
  const stars = Array(5).fill().map((_, i) => <IoStarOutline key={i} color="grey" />);
  for (let i = 0; i < rating; i++) {
    stars[i] = <IoStarSharp key={i} color="yellow" />;
  }

  return (
    <div>
      {stars}
    </div>
  );
}

export default StarRating;
