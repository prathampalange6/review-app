import express from 'express';
import mongoose from "mongoose";
import dotenv from "dotenv";
import testRouter from './routes/review.route.js'
import pkg from 'body-parser';
import path from 'path';
const {json} = pkg;

const __dirname = path.resolve();
const app = express();

app.use(json());

dotenv.config();

const PORT = process.env.PORT || 7000;
const MONGOURL = process.env.MONGO_URL;

mongoose.connect(MONGOURL).then(() => {
    console.log("Database connected successfully.");
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
});
  
app.use("/api/reviewlist", testRouter)
app.use(express.static(path.join(__dirname, '/client/dist')))

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'client', 'dist', 'index.html'))
})