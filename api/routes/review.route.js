import express from "express";

const router = express.Router();
import ReviewModel from '../models/review.model.js';

router.get('/getreview', async (req, res) => {
    try {
      const reviewData = await ReviewModel.find();
      res.json(reviewData);
    } catch (error) {
      console.error('Error fetching data:', error);
      res.status(500).send(error);
    }
});

export default router;