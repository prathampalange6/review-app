import mongoose from "mongoose";

const reviewSchema = new mongoose.Schema({
    review_id: String,
    reviewer_name: String,
    content: String,
    raw_content: String,
    date: String,
    rating_review_score: Number,
    hotel_code: String,
    hotel_reply: String,
    source_language: String,
    source_hotel_code_: String,
    source_review_id: String,
    category: String,
    phrases: String,
    sentences: String,
    topic: String,
    sentiment: String,
    out_of: Number,
    review_url: String,
    source: {
      code: String,
      name: String,
      icon: String,
      image: String
    },
    bookmarked: Boolean,
    bookmark_pk: Number,
    analytics: [{
      category: String,
      topic: String,
      phrases: [String],
      sentences: [String],
      sentiment: String,
      highlight_indices: [[mongoose.Schema.Types.Mixed]]
    }],
    highlight_indices: [[mongoose.Schema.Types.Mixed]]
  }, { collection: 'review' });

const ReviewModel = mongoose.model('review', reviewSchema, 'review');

export default ReviewModel;